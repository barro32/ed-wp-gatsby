# Ed Miliano CMS website

WIP: Wordpress CMS and React frontend, server side rendered with Gatsby.

## Hosting

CMS will be hosted at wordpress.com and frontend at (hopefully) Gitlab pages. 

### TODO

* ~~hook up frontend to CMS~~
* everything else!
* CI/CD for Gatsby to use Gitlab Pages
* Make WP headless
* Develop website
* Add content to WP
* Transfer domain
